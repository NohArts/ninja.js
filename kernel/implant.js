var common = require('./amber/common.js');

var args = [];

if (process.argv.length < 2) {
        console.log("Must supply an organ's type !")
} else {
    args.push(process.argv[2]);

    if (process.argv.length < 3) {
        console.log("Must supply an organ's name !")
    } else {
        args.push(process.argv[3]);

        if (process.argv.length < 4) {
            args.push('default');
        } else {
            args.push(process.argv[4]);
        }
    }
}

common.kernel(args[0], args[1], args[2], function () {
    common.program(function (session) {
        console.log("#) Running implant successfully ...")
    });
});

