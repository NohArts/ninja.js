var pypi = require('pypi');
var client = new pypi.Client();

client.getPackageReleases("Django", function (versions) {
    console.log(versions); // [ '1.3.1', '1.3', '1.2.7', '1.2.6', '1.2.5', '1.2.4', '1.2.3', '1.2.2', '1.2.1', '1.2', '1.1.4', '1.1.3', '1.1.2', '1.0.4' ]
});

// client.getPackageRoles "Django", (roles) -> console.log roles
// client.getUserPackages "ubernostrum", (packages) -> console.log packages
// client.getReleaseData "vcs", "0.2.2", (data) -> console.log data

