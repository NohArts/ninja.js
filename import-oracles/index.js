if (process.env.NODE_ENV === 'production') require('newrelic');
var swig = require('swig');
var log = require('./lib/logger');
var Resolver = require('./lib/resolver');

var App = require('./lib/app');
var Dash = require('./lib/dashboard');

var PORT = process.env.PORT || 5000;

/************************************************************************/

function Packager (verse,alias) {
    this.verse    = verse;
    this.alias    = alias;

    try {
        this.manifest = require(this.rpath('manifest.yaml'));
    } catch (ex1) {
        try {
            this.manifest = require(this.rpath('manifest.json'));
        } catch (ex2) {
            this.manifest = {};
        }
    }

    try {
        this.releases = require(this.rpath('releases.json'));
    } catch (ex3) {
        this.releases = {
            'native': this.manifest.semver,
        };
    }

    this.managers = {};
}

Packager.prototype.rpath = function (target) {
    return __dirname+'/registry/'+this.verse+'/'+this.alias+'/'+target;
}

Packager.prototype.prefix = function (target) {
    return this.verse+'/'+this.alias+'/'+target;

    switch (this.verse) {
        case 'system':
            return '\@'+this.alias+'/'+target;
            break;

        case 'language':
            return '\='+this.alias+'/'+target;
            break;

        case 'feature':
            return '\~'+this.alias+'/'+target;
            break;

        case 'daemon':
            return '\-'+this.alias+'/'+target;
            break;

        case 'backend':
            return '\+'+this.alias+'/'+target;
            break;
    }

    return this.verse+'_'+this.alias+'/'+target;
}

Packager.prototype.resolvers = function () {
    var listing=[], prefix=null, handler=null;

    for (var idx in this.releases) {
        var obj = this.releases[idx];

        if (obj==null) {
            //console.log(this.releases,obj,idx);
        } else {
            //console.log(this.releases,obj.type,idx);

            switch (obj.type) {
                case 'http':
                case 'http-regex':
                case 'regex':
                    prefix = 'http-regex';
                    break;
                case 'stats':
                case 'npm-stats':
                    prefix = 'npm-stats';
                    break;
                default:
                    prefix = null;
                    break;
            }

            if (prefix!=null) {
                handler = require(__dirname+'/adapters/'+prefix);
            } else {
                handler = null;
            }

            if (handler!=null) {
                var key = idx;

                obj.name = key;
                //obj.name = this.alias;

                listing.push([
                    this.prefix(key),
                    new handler(obj),
                ]);
            }
        }
    }

    return listing;
};

var cfg = {
    port: process.env.PORT || 5000,
    mapping: {
        system: [
            'ubuntu',
        ],
        language: [
            'python','nodejs',
        ],
        feature: [
            //'docker','hadoop',
        ],

        daemon: [
            //'supervisor',
            'nginx', //'apache2',
        ],
        backend: [
            //'redis',
            'mongodb',//'postgres','mysql',
            //'neo4j',
            //'solr','elasticsearch',
            //'rabbitmq','mqtt',
            //'fuseki','rdf4j',
        ],
        //*/
    },
    registers: {},
    resolvers: {},
    pipelines: [],
};

console.log("Loading plugins :");

for (verse in cfg.mapping) {
    cfg.registers[verse] = [];

    console.log("\t*) "+verse+" :");

    for (var i=0 ; i<cfg.mapping[verse].length ; i++) {
        var key = cfg.mapping[verse][i];

        console.log("\t\t-> "+key);

        var obj = new Packager(verse,key);

        cfg.registers[verse].push(obj);
    }
}

console.log("Registering plugins :");

for (verse in cfg.registers) {
    console.log("\t*) "+verse+" :");

    for (idx in cfg.registers[verse]) {
        var pkg = cfg.registers[verse][idx];

        console.log("\t\t-> "+pkg.alias);

        var lst = pkg.resolvers();

        for (var j=0 ; j<lst.length ; j++) {
            var target = lst[j][0];
            var handle = lst[j][1];

            console.log("\t\t\t:> "+target);

            cfg.resolvers[target] = new Resolver(handle);
        }
    }
}

/************************************************************************/

var app = new App(cfg);

/*
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/templates');
// Optional: use swig's caching methods
// app.set('view cache', false);
//*/

app.listen(PORT, function onListen() {
  log({ message: 'listening', port: cfg.port });
});

