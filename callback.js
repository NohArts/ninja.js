exports.RDF = {
    starts: function (store) {
        store.subscribe("http://example/book",null,null,null,exports.RDF.handler);
    },
    finish: function (store) {
        store.unsubscribe(exports.RDF.handler);
    },
    handler: function (event, triples) {
        // it will receive a notifications where a triple matching
        // the pattern s:http://example/boogk, p:*, o:*, g:*
        // is inserted or removed.
        if(event === 'added') {
            console.log(triples.length+" triples have been added");
        } else if(event === 'deleted') {
            console.log(triples.length+" triples have been deleted");
        }
    },
};

exports.IO = {
    starts: function (session) {
        session.subscribe('linked.rdf.import', exports.IO.topics.rdf_import);
        session.subscribe('linked.rdf.infered', exports.IO.topics.rdf_infer);
        session.subscribe('linked.sparql.log', exports.IO.topics.sparql_log);
    },
    finish: function (reason, details) {
        console.log("Connection lost: ", reason, details);
    },
    topics: {
        rdf_import: function onevent(args) {
            console.log("RDF_IN:", args);
        },
        rdf_infer: function onevent(args) {
            console.log("INFER:", args);
        },
        sparql_log: function onevent(args) {
            console.log("SPARQL:", args);
        },
    },
};

