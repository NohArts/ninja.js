var Say      = require('say');

exports.events = {
    init: function (config) {
        
    },
    open: function (session) {
        
    },
    close: function (reason, details) {
        //callbacks.IO.finish(reason, details);
    },
};

exports.methods = {
    '<sense>.speak': function (args) {
        var text  = args[0];
        var voice = args[1] || undefined;
        var speed = args[2] || 2.0;

        Say.speak(text, voice, speed, function(error) {
            exports.publish('<sense>.saying', [text, voice, speed]);

            if (error) {
                exports.publish('<sense>.dyslex', [text, voice, speed, error]);
                return console.error('Error speaking!', error);
            }

            exports.publish('<sense>.said', [text, voice, speed]);
        });
    },
    '<sense>.shutup': function (args) {
        Say.stop(function(err) {
            if (err) {
                exports.publish('<sense>.jabber', [err]);

                return console.error('unable to stop speech', err);
            } else {
                console.log('stopped');
            }
        });
    },
};

exports.topics = {
};

exports.program = function (session) {
    exports.invoke('<sense>.speak', [
        'This is Ground Control to Major Tom This is Ground Control to Major Tom This is Ground Control to Major Tom',
        undefined,
        2.0,
    ]).then(function (res) {
        console.log("Result:", res);
    });
};

