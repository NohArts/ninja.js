exports.events = {
    init: function (type, name, handler) {
        exports.wrapper = new require('speakable')({
            key: exports.creds.apis.google.token,
        }, {
            key: exports.perso.profile.language,
            lang: exports.profile.language,
        });
    },
    open: function (session) {
        
    },
    close: function (reason, details) {
        //callbacks.IO.finish(reason, details);
    },
};

exports.methods = {
};

exports.topics = {
};

exports.program = function (session) {
    var state = false;

    function ensure () {
        if (state==true) {
            exports.wrapper.recordVoice();
        } else {
            exports.wrapper.recordVoice();
        }
    }

    exports.wrapper.on('speechStart', function() {
        exports.publish('<sense>.state', ['start'])
    });

    exports.wrapper.on('speechStop', function() {
        exports.publish('<sense>.state', ['stop'])
        console.log('onSpeechStop');
    });

    exports.wrapper.on('speechReady', function() {
        exports.publish('<sense>.state', ['ready'])
    });

    exports.wrapper.on('error', function(err) {
        exports.publish('<sense>.error', [err])

        ensure();
    });

    exports.wrapper.on('speechResult', function(recognizedWords) {
        exports.publish('<sense>.hear', [recognizedWords])

        ensure();
    });

    session.register('<sense>.listen', function (args) {
        state = true;

        return exports.wrapper.recordVoice();
    });
    session.register('<sense>.numb', function (args) {
        state = false;

        return exports.wrapper.resetVoice();
    });
};

exports.on_close = function (reason, details) {
    //callbacks.IO.finish(reason, details);
}

