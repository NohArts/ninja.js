var cv = require('opencv');

exports.vars = {};

exports.events = {
    init: function (config) {
        exports.vars.camera = new cv.VideoCapture(0);
        exports.vars.window = new cv.NamedWindow('Video', 0)
    },
    open: function (session) {
        // callbacks.IO.starts(session);
    },
    close: function (reason, details) {
        //callbacks.IO.finish(reason, details);
    },
};

exports.methods = {
    '<sense>.see': function (args) {
        try {
            exports.vars.camera.read(function(err, im) {
                if (err) throw err;

                if (im.size()[0] > 0 && im.size()[1] > 0){
                    exports.publish('<sense>.image', [im.size()]);

                    im.detectObject(cv.FACE_CASCADE, {}, function(err, faces) {
                        if (!(err)) {
                            for (var i=0;i<faces.length; i++){
                                var x = faces[i];
                                im.ellipse(x.x + x.width/2, x.y + x.height/2, x.width/2, x.height/2);
                            }
                            exports.publish('<sense>.face', [faces[i]]);
                        }

                        exports.vars.window.show(im);
                    });
                }

                exports.vars.window.blockingWaitKey(0, 50);
            });
        } catch (e){
            console.log("Couldn't start camera:", e)
        }
    },
};

exports.topics = {
    '<sense>.image': function (args) {
    },
    '<sense>.face': function (args) {
    },
};

exports.program = function (session) {

};

