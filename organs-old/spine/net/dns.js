var dns = require('native-dns');

exports.port = 60053;

exports.events = {
    init: function (config) {
        exports.serv = dns.createServer();

        exports.serv.on('request', exports.events.request);

        exports.serv.on('error', exports.events.error);
    },
    open: function (session) {
        // callbacks.IO.starts(session);
    },
    close: function (reason, details) {
        //callbacks.IO.finish(reason, details);
    },
    request: function (request, response) {
        exports.invoke('reactor.spine.net.dns.resolve', [
            request.question[0].name,
            'A',
        ]).then(function (res) {
            var i;

            for (i=0 ; i<res.primary.length ; i++) {
                res.primary[i].type = res.primary[i].type || 'A';

                rec = dns[res.primary[i].type];

                response.answer.push(rec({
                    name: res.primary[i].fqdn,
                    address: res.primary[i].addr,
                    ttl: res.primary[i].ttl,
                }));
            }

            for (i=0 ; i<res.secondary.length ; i++) {
                res.secondary[i].type = res.secondary[i].type || 'A';

                rec = dns[res.secondary[i].type];

                response.additional.push(rec({
                    name: res.secondary[i].fqdn,
                    address: res.secondary[i].addr,
                    ttl: res.secondary[i].ttl,
                }));
            }

            response.send();
        }, function (err) {
            response.send();
        });
    },
    error: function (err, buff, req, res) {
      console.log(err.stack);
    },
};

exports.program = function (session) {
    console.log("Running HTTP server on port "+exports.port+" ...");

    server.serve(exports.port);
};

exports.methods = {
    'reactor.spine.net.dns.resolve': function (fqdn, type, host) {
        type = type || 'A';
        host = host || '8.8.8.8';

        dns.resolve(fqdn, type, host, function (err, results) {
            var i;

            if (!err) {
                var resp = {
                    primary: [],
                    secondary: [],
                };

                for (i = 0; i < results.length; i++) {
                    var entry = {
                        'fqdn': fqdn,
                        'type': type,
                        'addr': results[i],
                        'time': 600,
                    };

                    exports.publish('reactor.spine.net.dns.primary', {
                        'fqdn': fqdn,
                        'type': type,
                        'host': host,
                    }, entry);

                    resp.primary.push(entry);
                }

                return resp;
            } else {
                console.log(err);
            }
        });
    },
};

exports.topics = {
    'reactor.spine.net.dns.primary': function (query, entry) {
        // console.log(fqdn, results[i]);
    },
};

